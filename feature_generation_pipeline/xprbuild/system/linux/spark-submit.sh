#! /bin/bash
## This script is used to build the project.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user

export PYSPARK_PYTHON=python3
export PYSPARK_DRIVER_PYTHON=python3
export JAVA_HOME=/usr/jdk64/jdk1.8.0_112
export HADOOP_CONF_DIR=/etc/hadoop/conf
export CLASSPATH=/root/experiments/horovod/sample/spark-2.4.4-bin-hadoop2.7/jars
export SPARK_HOME=/root/experiments/horovod/sample/spark-2.4.4-bin-hadoop2.7


if [[ $1 == "pkg" ]]
then
	echo "Generating dependency packages"
	cd ${ROOT_FOLDER}/..
	zip -qr packs.zip ./*
	mv packs.zip ${ROOT_FOLDER}/packs.zip
	cd ${ROOT_FOLDER}
	echo "Packagse generated"
else
	spark-submit --master yarn \
	       --py-files packs.zip \
	       --name ${JOB_NAME} \
	       --deploy-mode cluster \
	       --executor-memory ${EXECUTOR_MEMORY} \
	       --executor-cores ${EXECUTOR_CORES}  \
	       --num-executors ${NUM_EXECUTORS} \
	       --driver-memory ${DRIVER_MEMORY} \
	       app/main.py ${CMD_ARGS}
fi